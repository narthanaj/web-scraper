FROM python:3.9-slim

# Install dependencies
RUN apt-get update && apt-get install -y \
    wkhtmltopdf \
    && rm -rf /var/lib/apt/lists/*

# Set up environment
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt

# Copy script
COPY scraper.py scraper.py

# Run script
ENTRYPOINT ["python", "scraper.py"]