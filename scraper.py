# scraper.py

import requests
from bs4 import BeautifulSoup
import pdfkit
import logging
import sys

logging.basicConfig(level=logging.INFO)

def scrape_website(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        soup = BeautifulSoup(response.content, 'html.parser')
        return str(soup)
    except requests.RequestException as e:
        logging.error(f"Error scraping {url}: {e}")
        return None

def convert_to_pdf(content, output_filename):
    try:
        pdfkit.from_string(content, output_filename)
    except Exception as e:
        logging.error(f"Error converting content to PDF: {e}")

def main(target_url):
    logging.info(f"Scraping content from {target_url}")
    content = scrape_website(target_url)
    if content:
        logging.info("Converting scraped content to PDF")
        convert_to_pdf(content, 'output.pdf')
        logging.info("PDF saved as output.pdf")
    else:
        logging.error("Failed to scrape content")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        logging.error("Usage: python scraper.py <URL>")
        sys.exit(1)
    target_url = sys.argv[1]
    main(target_url)
